import { Link, NavLink } from "react-router-dom";
import { Container, Nav, Navbar,NavDropdown } from 'react-bootstrap';
import {useState, useContext,useEffect} from "react";
import {UserContext,TokenContext} from '../UserContext';

export default function AppNavBar() {
  const {
    userId,
    userName,
    setUserName,
    isSeller,
    setIsSeller,
    setToken
  } = useContext(UserContext);

  useEffect(() => {
    console.log(isSeller);
  }, [isSeller]);
    
  return (
    <Navbar bg="light" expand="lg">
      <Container>
        <Navbar.Brand as={Link} to={'/'}>SilkRoad</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link as={NavLink} to="/">Home</Nav.Link>
            {!userName&& <Nav.Link as={NavLink} to={'/register'}>Register</Nav.Link>}
            {!userName&& <Nav.Link as={NavLink} to={'/login'}>Login</Nav.Link>}
            {isSeller && <Nav.Link as={NavLink} to={'/admin/newProduct'}>NewProducts</Nav.Link>}
            {isSeller && <Nav.Link as={NavLink} to={'/admin/newProduct'}>I'm a seller</Nav.Link>}
            {isSeller && <Nav.Link as={NavLink} to={'/admin/products'}>MyProducts</Nav.Link>}
            {userName &&
              <NavDropdown title={userName} id="userDropDown">
              <NavDropdown.Item  as={NavLink} to={'/users/checkout'}>Checkout</NavDropdown.Item>
              <NavDropdown.Item  as={NavLink} to={'/dashboard'}>Dashboard</NavDropdown.Item>
              {isSeller&&<NavDropdown.Item  as={NavLink} to={'/orders'}>Orders</NavDropdown.Item>}
              <NavDropdown.Divider />
              <NavDropdown.Item  as={NavLink} to={'/settings'}>Settings</NavDropdown.Item>
              <NavDropdown.Item  as={NavLink} to={'/logout'}>Logout</NavDropdown.Item>
            </NavDropdown>
            }
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}