import { useEffect, useState,useContext } from 'react';
import './scss/App.css';
import {UserContext,TokenContext} from './UserContext'

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
// components
import AppNavBar from './components/AppNavBar';
//pages
// import NewProduct from './pages/admin/Admin_AddNewProduct';
import AddSku from './pages/admin/Admin_AddSku';
import Admin_Dashboard from './pages/admin/Admin_Dashboard';
import AdminOrders from './pages/admin/Admin_Orders';
import AdminProducts from './pages/admin/Admin_Products'

import UserDashboard from './pages/users/User_Dashboard';
import Checkout from './pages/users/Checkout';
import RecoverPassword from './pages/users/RecoverPassword';
import Register from './pages/Register';

import Dashboard from './pages/Dashboard';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import PrintOnDemand from './pages/PrintOnDemand';
import ProductCatalog from './pages/ProductCatalog';
import NotFound from './pages/NotFound';
import AdminAddNewProduct from './pages/admin/AdminAddNewProduct';

import IsLogin from './util/IsLogin';
import AdminViewProduct from './pages/admin/AdminViewProduct';


function App() {

  const [userName, setUserName] = useState()
    const unSetUserName = () => {
    localStorage.removeItem('userName');
    setUserName(null);
  }
  const [userId, setUserId] = useState()
    const unSetUserId = () => {
    localStorage.removeItem('id');
    setUserId(null);
  }
  const [isSeller, setIsSeller] = useState()  
    const unSetisSeller = () => {
    localStorage.removeItem('isSeller');
    setIsSeller(null);
  }

  const [token, setToken] = useState()
  const unSetToken = () => {
    localStorage.removeItem('token');
    setToken(null);
  }



// useEffect(() => {
//   setIsSeller(localStorage.getItem('isSeller'))
//   setUserName(localStorage.getItem('userName'))
//   setUserId(localStorage.getItem('id'))
//   setToken(localStorage.getItem('token'))
// },
//     [localStorage.getItem('isSeller')
//     ,localStorage.getItem('userName')
//     ,localStorage.getItem('id')
//     ,localStorage.getItem('token')]);

useEffect(() => {
    setUserName(localStorage.getItem('userName'))
    setUserId(localStorage.getItem('id'))
    setIsSeller(localStorage.getItem('isSeller'))
    setToken(localStorage.getItem('token'))
  }, [])
  // useEffect(() => {

  //   fetch(`${process.env.REACT_APP_URL}/user/viewprofile`, {
  //     headers: {
  //       Authorization: `Bearer ${localStorage.getItem('token')}`
  //     }
  //   })
  //     .then(res => {
  //       return res.json()
  //     })
  //     .then(data => {
  //       console.log(`Data in app`, data);
  //       setUser({ id: data._id, isAdmin: data.isAdmin });
  //       console.log(user);

  //     })
  //   localStorage.getItem('token')
  // }, [])



  return (
    <>
      {/* <UserProvider> */}
      <UserContext.Provider value={{
        userName, setUserName, unSetUserName,
        userId, setUserId, unSetUserId,
        setIsSeller, isSeller, unSetisSeller,
        token,setToken,unSetToken }}>
        <Router>
          <AppNavBar />
          <Routes>
            <Route path="/admin" element={<IsLogin/>}>
              {/* <Route index element={<Admin_Dashboard/>} /> */}
              <Route path="newProduct" element={<AdminAddNewProduct/>} />
              <Route path="addSku" element={<AddSku />} />
              <Route path="orders" element={<AdminOrders />} />
              <Route path="products/sku/:seller/:prodid" element={<AdminViewProduct/>} />
              <Route path="products" element={<AdminProducts />} />
            </Route>
            <Route path="/users" element={<IsLogin/>}>
              <Route index element={<UserDashboard/>} />
              <Route path="checkout" element={<Checkout />} />
              <Route path="recoverPassword" element={<RecoverPassword />} />
            </Route>

            <Route path="seller">
              <Route path=":id/prodId/:prodId" element={<Home />} />
              {/* <Route path="/seller/findBucket" element={<Home />} /> */}
            </Route>
            <Route path="/" element={<Home />} />
            <Route path="login" element={<Login />} />
            <Route path="logout" element={<Logout />} />
            <Route path="*" element={<NotFound />} />
            <Route path="printondemand" element={<PrintOnDemand/>} />
            <Route path="dashboard" element={<ProductCatalog/>} />
            <Route path="register" element={<Register />} />
            {/* <Route path="PrintOnDemand" element={<PrintOnDemand />} />
            <Route path="ProductCatalog" element={<ProductCatalog />} /> */}
          </Routes>
        </Router>
      </UserContext.Provider>
    </>
  );
}
export default App;
