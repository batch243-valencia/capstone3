import { useState, useEffect, useContext } from 'react';
import {Button, Form, Col, Row, Container} from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import {UserContext} from '../UserContext';
import FetchUtil from '../util/FetchUtil';
import swalFire from '../util/swalFire';
// const username=false
//  let baseURL = "http://localhost:4000"
// export default function Login() {
// const [email,setEmail] = useState('');
// const [password,setPassword] = useState('');
// const [inActive, setInActive] = useState(true);
// const[user,setUser] = useState('')

// // const navigate = useNavigate()
// useEffect(()=>{
//         // email && console.log(email)
//         // password && console.log(password)
//         setInActive(
//             email && 
//             password)
//     },[email,password]
// )
// // useEffect(()=>{
// //  navigate(-1)
// //     },[]
// // )

// async function loginUser(credentials) {
//     console.log(credentials)
//   return fetch('http://localhost:4000/user/login', {
//     method: 'POST',
//     headers: {
//       'Content-Type': 'application/json'
//     },
    
//     body: JSON.stringify(credentials)
//   })
//     .then(data => data.json())
    
//  }  

async function loginUser(credentials) {
    // console.log(credentials)
  const response = await FetchUtil('http://localhost:4000/user/login',"Post",credentials)
//     const response = await fetch('http://localhost:4000/user/login', {
//     method: 'POST',  
//     mode:'cors',
//     headers: {
//       'Content-Type': 'application/json',
//       'Accept': 'application/json'
//    },
// //    body: JSON.stringify(credentials)
//    body: JSON.stringify(credentials)
//  })
 try {
  const result = await response.json()
  return result
 } catch (error) {
   return false
 }
}
export default function Login() {
  // eslint-disable-next-line react-hooks/rules-of-hooks
const {
        userName, setUserName,
        userId, setUserId,
        setIsSeller,
        setToken
      } =useContext(UserContext);
// eslint-disable-next-line react-hooks/rules-of-hooks
const [userInput, setuserInput] = useState();
const [password, setPassword] = useState();
  const  handleSubmit = async  e => {
    e.preventDefault();
    // console.log(userInput,password)
    const result =  await loginUser({
      userInput,
      password
    });
    if(!result){
      swalFire("Authentication Failed","error", "Wrong credentials, please try again!")
      return
      }
    localStorage.setItem('token', result.token);
    localStorage.setItem('userName', result.user.userName);
    localStorage.setItem('isSeller', result.user.isSeller);
    localStorage.setItem('id', result.user._id);
    setToken(result.token);
    setUserId(result.user._id);
    setUserName(result.user.userName);
    setIsSeller(result.user.isSeller);
      
    swalFire(`Login Successful`,`success`,`Welcome ${userName} to our website!`)
  }
return (
      (userName)
        ?<Navigate to ="/"/>
        :
    <>
        <Form  onSubmit={handleSubmit}>
            <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control type="text" placeholder="Enter Username, Email or Phone Number"
                onChange={e => setuserInput(e.target.value)}  />
                {/* <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text> */}
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" placeholder="Password"
                 onChange={e => setPassword(e.target.value)} />
            </Form.Group>
            <Button variant="primary" type="submit">
                Submit
            </Button>
        </Form>
    </>
)
}

