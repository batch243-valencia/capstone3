import { Navigate } from "react-router-dom"
import {UserContext} from '../UserContext';
import react,{useContext, useEffect,useState} from 'react'
import Swal from 'sweetalert2';

export default function Logout() {
    // localStorage.clear()
 
const {
        setUserName,unSetUserName,
        setIsSeller,unSetisSeller,
        setToken,unSetToken
      } =useContext(UserContext);
    localStorage.removeItem('id')
    localStorage.removeItem('token');
    localStorage.removeItem('userName');
    localStorage.removeItem('isSeller');
    
    useEffect(()=>{
        // setUserName(null)
        unSetUserName(null);
        // setIsSeller(null)
        unSetisSeller(null);
        // setToken(null)
        unSetToken(null)
    })
    
    Swal.fire({
        title:"Logged Out",
        icon: "info"
    })
    return ( 
        <Navigate to ="/login"/>
     );
}
 
