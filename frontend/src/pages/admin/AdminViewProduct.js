import FetchUtil from "../../util/FetchUtil"
import { useParams } from 'react-router-dom';
import { useContext, useEffect, useState } from "react";
import {userContext} from "../../UserContext"

export default function AdminViewProduct() {
    const {seller, setSeller} = useParams();
    const {prodid, setprodid} = useParams();
    const [response, setResponse] = useState([])
    const [error, setError] = useState(null)

    // const navigate = useNavigate();
        const [data, setData] = useState([]);
        const [loading, setLoading] = useState(true);
        const [IsProductEmpty,setIsProductEmpty] = useState(false)
   useEffect(() => {
            const sellerId=localStorage.getItem('id')
            // console.log(`http://localhost:4000/products/seller/${sellerId}/`)
            async function fetchData() {
                try {
                    const response = await fetch(`${process.env.REACT_APP_API_URL}/products/seller/${sellerId}`);
                    const data = await response.json();
                    // console.log(data.length)
                    // console.log(data)
                    // console.log(response)
                    setData(data);
                    setLoading(false);
                    setIsProductEmpty(false)
                    // console.log(response.length)
                    
                } catch (error) {
                    setLoading(false);
                    setIsProductEmpty(true)
                }
            }
            fetchData();
        }, []);
     return(
        <>
        {error && <p>{error}</p>}
        {response && Array.isArray(response) ? 
        <table className="table">
            <thead>
                <tr>
                    <th>Product Name</th>
                    <th>Product Description</th>
                    <th>Product Price</th>
                </tr>
            </thead>
            <tbody>
                {response.map((product) => (
                    <tr key={product._id}>
                        <td>{product.name}</td>
                        <td>{product.description}</td>
                        <td>{product.price}</td>
                    </tr>
                ))}
            </tbody>
        </table> 
        : <div>{response ? "No products found" : "Loading..."}</div>}
        </>
    )
}