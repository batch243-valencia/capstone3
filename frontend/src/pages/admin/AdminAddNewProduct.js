import { Button, Col, Container, Form, Row } from "react-bootstrap"
import { useState, useEffect, useContext } from 'react';
import Swal from 'sweetalert2';
import { Navigate } from "react-router-dom";
import FetchUtil from '../../util/FetchUtil';

export default function AdminAddNewProduct(productDetails) {
    const [status, setStatus] = useState(
        {
            submitting: false,
            success: false,
            error: false
        });
    const [seller, setSeller] = useState('');
    const [bucket, setbucket] = useState('');
    const [productName, setproductName] = useState('');
    const [description, setDescription] = useState('');
    const [category, setCategory] = useState('');
    const [type, setType] = useState('');
    const [material, setMaterial] = useState('');


    productDetails = {
        seller,
        bucket,
        productName,
        description,
        category,
        type,
        material
    }

    //   const history = Navigate();

    const handleSubmit = async (event) => {
        // console.log("handleSubmit")
        event.preventDefault();
        setStatus({ ...status, submitting: true })
        // console.log(productDetails)
        const id = localStorage.getItem('id');
        const token = localStorage.getItem('token');
        /* Simple FetchUtility -FetchUtil(
                    url = '',
                    method="GET",
                    data={},
                    token)
                    */
        try {
            console.log("try")
            const response = await FetchUtil(`${process.env.REACT_APP_API_URL}/products/seller/${id}/create`
                , "POST",
                productDetails,
                token)
                console.log(response)
                    if(response.status !== 201){
        throw new Error(response.error);
    }

        if (response.error) {
        if (response.error.includes("duplicate key error collection")) {
            throw new Error(`Error Adding "${productName}" is already been taken`);
        } else {
            throw new Error(response.error);
        }
    }
            await Swal.fire({
                title: 'Success!',
                text: 'The product was added successfully.',
                icon: 'success',
                confirmButtonText: 'OK'
            })
            // console.log(response?.status);    
        } catch (error) {
             console.log("error")
    console.log(error.message)
            await Swal.fire({
                title: 'Error!',
                text: `Error Adding "${productName}" is already been taken`,
                icon: 'error',
                confirmButtonText: 'OK'
            });
        }
        await setStatus({ ...status, error: false, submitting: false })
        //    setStatus({})
        // console.log(status)
        //   if (response?.isDuplicate === true) {
        //     console.log("asd")

        //       setStatus({ ...status, error: true })
        //   } else if (response.status === 200) {
        //     const data = await response.json();
        //     Swal.fire({
        //       title: 'Success!',
        //       text: 'The product was added successfully.',
        //       icon: 'success',
        //       confirmButtonText: 'OK'
        //     });
        //       setStatus({ ...status, success: true })
        //   }
        //   console.log(response.status)
        // if (response ===false)return 
        // } catch (error) {
        //   Swal.fire({
        //       title: 'Error!',
        //       text: "Error Duplicate Entry",
        //       icon: 'error',
        //       confirmButtonText: 'OK'
        //     });
        // }
    }

    return (
        <Container>
            <Row>
                <Col className='offset-4 mt-4' xs={12} md={6} lg={4}>
                    <Form className='bg-secondary p-3' onSubmit={handleSubmit}>
                        {/* seller */}
                        <Form.Group className="mb-3 " controlId="seller">
                            <Form.Label>{localStorage.getItem('userName')}</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="seller"
                                value={localStorage.getItem('userName')}
                                onChange={event => {
                                    setSeller(event.target.value)
                                }}
                                required
                                hidden
                            />
                        </Form.Group>
                        {/*bucket*/}
                        <Form.Group className="mb-3 " controlId="bucketName">
                            <Form.Label>Bucket Name</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Product name"
                                value={bucket}
                                onChange={event => {
                                    setbucket(event.target.value)
                                }}
                                required
                                disabled={status.submitting}
                            />
                        </Form.Group>
                        {/* Product Name */}
                        <Form.Group className="mb-3 " controlId="productName">
                            <Form.Label>Product Name</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Product name"
                                value={productName}
                                onChange={event => {
                                    setproductName(event.target.value)
                                }}
                                required
                                disabled={status.submitting}
                            />
                        </Form.Group>
                        {/* Description */}
                        <Form.Group className="mb-3 " controlId="description">
                            <Form.Label>Description</Form.Label>
                            <Form.Control
                                as="textarea" row={3}
                                placeholder="description"
                                value={description}
                                onChange={event => {
                                    setDescription(event.target.value)
                                }}
                                required
                                disabled={status.submitting}
                            />
                        </Form.Group>
                        {/* category */}
                        <Form.Group className="mb-3 " controlId="category">
                            <Form.Label>Category</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="category"
                                value={category}
                                onChange={event => {
                                    setCategory(event.target.value)
                                }}
                                required
                                disabled={status.submitting}
                            />
                        </Form.Group>
                        {/* type */}
                        <Form.Group className="mb-3 " controlId="type">
                            <Form.Label>Type</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="type"
                                value={type}
                                onChange={event => {
                                    setType(event.target.value)
                                }}
                                required
                                disabled={status.submitting}
                            />
                        </Form.Group>
                        {/* materials */}
                        <Form.Group className="mb-3 " controlId="materials">
                            <Form.Label>material</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="materials"
                                value={material}
                                onChange={event => {
                                    setMaterial(event.target.value)
                                }}
                                required
                                disabled={status.submitting}
                            />
                        </Form.Group>

                        <div className='text-center'>
                            <Button
                                className="btn-lg"
                                variant="primary"
                                type="submit"
                                disabled={status.submitting}>
                                Create Product
                            </Button>
                        </div>
                    </Form>
                </Col>
            </Row>
        </Container>

    )
};
