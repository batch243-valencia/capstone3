    import { useState, useEffect, useContext } from 'react';
    import { Button, Form, Col, Row, Container } from 'react-bootstrap';
    import { Navigate, redirect, useNavigate } from 'react-router-dom';
    import Swal from 'sweetalert2';
    import { UserContext } from '../../UserContext';
    import { Link } from "react-router-dom";
    import { Table, Spinner } from 'react-bootstrap';
    // import { useNavigate } from 'react-router';
    import { ToggleButton, ToggleButtonGroup } from 'react-bootstrap';

    export default function Admin_Products() {
        

    const navigate = useNavigate();
        const [data, setData] = useState([]);
        const [loading, setLoading] = useState(true);
        const [IsProductEmpty,setIsProductEmpty] = useState(false)
        
        useEffect(() => {
            const sellerId=localStorage.getItem('id')
            // console.log(`http://localhost:4000/products/seller/${sellerId}/`)
            async function fetchData() {
                try {
                    const response = await fetch(`${process.env.REACT_APP_API_URL}/products/seller/${sellerId}`);
                    const data = await response.json();
                    // console.log(data.length)
                    // console.log(data)
                    // console.log(response)
                    setData(data);
                    setLoading(false);
                    setIsProductEmpty(false)
                    // console.log(response.length)
                    
                } catch (error) {
                    setLoading(false);
                    setIsProductEmpty(true)
                }
            }
            fetchData();
        }, []);

        return (
            <div>
                {loading ? (
                    <Spinner animation="border" variant="primary" />
                ) : (
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                                <th>{localStorage.getItem('userName')}</th>
                                <th>Bucket</th>
                                <th>Product Name</th>
                                <th>Description</th>
                                <th>Category</th>
                                <th>Type</th>
                                <th>Materials</th>
                            </tr>
                        </thead>
                        <tbody>
                            {IsProductEmpty
                            ?<tr><td colSpan={7} className="text-center">No Product Found</td></tr>
                            :null}
                            {data.map((fetchData) => (
                                <tr key={fetchData._id}>
                                    <td>{fetchData._id}</td>
                                    <td>{fetchData.bucket}</td>
                                    <td>{fetchData.productName}</td>
                                    <td>{Array.isArray(fetchData.description) ? fetchData.description.join(', ') : fetchData.description}</td>
                                    <td>{fetchData.category}</td>
                                    <td>{fetchData.type}</td>
                                    <td>{Array.isArray(fetchData.materials) ? fetchData.materials.join(', ') : fetchData.materials}</td>
                                    <td><Button variant="primary" onClick={() => navigate(`seller/${fetchData.seller}/prodid/${fetchData._id}`)}>View</Button>{' '}</td>
                                    <td><Button variant="secondary">Edit</Button>{' '}</td>
                                    <td><Button variant="success">Add</Button>{' '}</td>
                                    <td><Button variant="danger">Archive</Button>{' '}</td>
                                </tr>
                            ))}
                            
                        </tbody>
                    </Table>
                )}
            </div>
            //  <Table striped bordered hover>
            //   <thead>
            //     <tr>
            //       <th>{localStorage.getItem('userName')}</th>
            //       <th>Bucket</th>
            //       <th>Product Name</th>
            //       <th>Description</th>
            //       <th>Category</th>
            //       <th>Type</th>
            //       <th>Materials</th>

            //     </tr>
            //   </thead>
            //   <tbody id="product-table-body">

            //   </tbody>
            // </Table>
        )
    }
