import { useState, useEffect, useContext } from 'react';
import { Button, Form, Col, Row, Container } from 'react-bootstrap';
import { Navigate, redirect, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import { UserContext } from '../../UserContext';
import { Link } from "react-router-dom";
import { Table, Spinner } from 'react-bootstrap';
// import { useNavigate } from 'react-router';
import { ToggleButton, ToggleButtonGroup } from 'react-bootstrap';
export default function AdminViewSku(params) {
     const [data, setData] = useState([]);
    const [loading, setLoading] = useState(true);
    
    useEffect(() => {
        async function fetchData() {
            try {
                const response = await fetch('http://localhost:4000/products/seller/63ac32be522adafb42acba8d');
                const data = await response.json();
                setData(data);
                setLoading(false);
            } catch (error) {
                console.log(error);
            }
        }
        fetchData();
    }, []);
    
};
